package utils

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestUtils(t *testing.T) {

	t.Run("Area Test", func(t *testing.T) {
		perimeter := CalcTriangleArea(0, 0, 0, 1, 1, 0)
		assert.Equal(t, 0.49999999999999983, perimeter)
	})

	t.Run("Area Aligned Points", func(t *testing.T) {
		perimeter := CalcTriangleArea(0, 0, 0, 1, 0, 2)
		assert.Equal(t, perimeter, float64(0))
	})

	t.Run("Get Next X Position", func(t *testing.T) {
		xPosition := CalcNextXPosition(1, 500, 0)
		assert.Equal(t, float64(0), xPosition)
		xPosition = CalcNextXPosition(1, 500, 90)
		assert.Equal(t, float64(500), xPosition)
		xPosition = CalcNextXPosition(1, 500, 270)
		assert.Equal(t, float64(-500), xPosition)
	})

	t.Run("Get Next Y Position", func(t *testing.T) {
		xPosition := CalcNextXPosition(1, 500, 0)
		assert.Equal(t, float64(0), xPosition)
		xPosition = CalcNextXPosition(1, 500, 90)
		assert.Equal(t, float64(500), xPosition)
		xPosition = CalcNextXPosition(1, 500, 270)
		assert.Equal(t, float64(-500), xPosition)
	})

}
