package utils

import (
	"math"
	"math/big"
)

/*
Calculates tringle area.
Finds the value of the semi-perimeter of the given triangle. S = 1/2(a+b+c);
Uses Heron’s formula to find the area of a triangle.

 	@param x1 the x-axis .
 	@param y1 the y-axis .
 	@param x2 the x-axis .
 	@param y2 the y-axis .
 	@param x3 the x-axis .
 	@param y3 the y-axis .
 	@return Area of the triangle
*/
func CalcTriangleArea(x1, y1, x2, y2, x3, y3 float64) float64 {
	var sideA = CalcDistance(x1, y1, x2, y2)
	var sideC = CalcDistance(x1, y1, x3, y3)
	var sideB = CalcDistance(x2, y2, x3, y3)
	var semiPerimeter = 0.5 * (sideA + sideB + sideC)
	var area = math.Sqrt(
		math.Abs(semiPerimeter) *
			math.Abs(semiPerimeter-sideA) *
			math.Abs(semiPerimeter-sideB) *
			math.Abs(semiPerimeter-sideC))
	return area
}

/*
Calculates triangle perimeter
 	@param x1 the x-axis .
 	@param y1 the y-axis .
 	@param x2 the x-axis .
 	@param y2 the y-axis .
 	@param x3 the x-axis .
 	@param y3 the y-axis .
 	@return Perimeter of the triangle
*/
func CalcTrianglePerimeter(x1, y1, x2, y2, x3, y3 float64) float64 {
	var sideA = CalcDistance(x1, y1, x2, y2)
	var sideB = CalcDistance(x1, y1, x3, y3)
	var sideC = CalcDistance(x2, y2, x3, y3)
	var semiPerimeter = 0.5 * (sideA + sideB + sideC)
	var area = math.Sqrt(
		math.Abs(semiPerimeter) *
			math.Abs(semiPerimeter-sideA) *
			math.Abs(semiPerimeter-sideB) *
			math.Abs(semiPerimeter-sideC))
	if area == 0 {
		return 0
	}
	return semiPerimeter * 2
}

/*
Calculate hypotenuse to find distance between two coordinates
*/
func CalcDistance(x1, y1, x2, y2 float64) float64 {
	var distance = math.Hypot(x2-x1, y2-y1)
	return distance
}

/*
Converts angle tranlsation from Degress to Radians
Trigonometric functions work with radians rather than degrees

	@param x1 the axis-x for the first triangle point.
	@return the triangle perimeter.

*/
func toRadians(degree float64) float64 {
	return degree * (math.Pi / 180)
}

/**
 * Calculates X-axis Position
 	* @param velocity The angle translation speed specified in degrees/day.
	* @param distance The distance from the planet to the Sun in kilometers.
 	* @param days number of days to translate.
 	* @return Next Y-axis Position in kilometers.
*/
func CalcNextXPosition(velocity, distance, days float64) float64 {
	var degrees = velocity * days
	var relativeDregrees = math.Mod(degrees, 360)
	result := big.NewFloat(degrees).Cmp(big.NewFloat(0))
	if result < 0 {
		relativeDregrees = 360 - math.Mod(math.Abs(degrees), 360)
	}
	if relativeDregrees >= 0 && relativeDregrees <= 90 {
		var position = math.Sin(toRadians(relativeDregrees)) * distance
		return position
	} else if relativeDregrees > 90 && relativeDregrees <= 180 {
		var position = math.Cos(toRadians(relativeDregrees-90)) * distance
		return position
	} else if relativeDregrees > 180 && relativeDregrees <= 270 {
		var position = -math.Sin(toRadians(relativeDregrees-180)) * distance
		return position
	} else {
		var position = -math.Cos(toRadians(relativeDregrees-270)) * distance
		return position
	}
}

/**
 * Calculates Y-axis Position
 	* @param velocity The angle translation speed specified in degrees/day
	* @param distance The distance from the planet to the Sun in kilometers
 	* @param days number of days to translate.
 	* @return Next Y-axis Position in kilometers.
*/
func CalcNextYPosition(velocity, distance, days float64) float64 {
	var degrees = velocity * days
	var relativeDregrees = math.Mod(degrees, 360)
	result := big.NewFloat(degrees).Cmp(big.NewFloat(0))
	if result < 0 {
		relativeDregrees = 360 - math.Mod(math.Abs(degrees), 360)
	}
	if relativeDregrees >= 0 && relativeDregrees <= 90 {
		var position = math.Cos(toRadians(relativeDregrees)) * distance
		return position
	} else if relativeDregrees > 90 && relativeDregrees <= 180 {
		var position = -math.Sin(toRadians(relativeDregrees-90)) * distance
		return position
	} else if relativeDregrees > 180 && relativeDregrees <= 270 {
		var position = -math.Cos(toRadians(relativeDregrees-180)) * distance
		return position
	} else {
		var position = math.Sin(toRadians(relativeDregrees-270)) * distance
		return position
	}
}
