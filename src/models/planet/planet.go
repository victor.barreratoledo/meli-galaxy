package planet

import (
	"victor-barrera.com/victorbarrera/galaxy-meli/src/utils"
)

// A Planet defines the properties required for a point in a circle orbit
type Planet struct {
	Name      string
	Distance  float64
	Velocity  float64
	XPosition float64
	YPosition float64
}

/*
Creates a Planet Instance

 	@param name planet name
 	@param distance planet distance to sun in kilometers
 	@param velocity planet translation velocity in Degrees/Day
 	@param xPosition planet initial x position
	@param yPosition planet initial y position
 	@return plant a Planet Instance
*/
func Create(name string, distance, velocity, xPosition, yPosition float64) *Planet {
	var planet = &Planet{
		Name:      name,
		Distance:  distance,
		Velocity:  velocity,
		XPosition: xPosition,
		YPosition: yPosition,
	}
	return planet
}

/*
Tranlate to n days the planet

 	@param planet planetInstance
*/
func (p *Planet) TranslateDays(days int) {
	p.XPosition = utils.CalcNextXPosition(p.Velocity, p.Distance, float64(days))
	p.YPosition = utils.CalcNextYPosition(p.Velocity, p.Distance, float64(days))
}
