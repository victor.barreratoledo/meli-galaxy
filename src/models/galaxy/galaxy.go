package galaxy

import (
	"sync"

	"victor-barrera.com/victorbarrera/galaxy-meli/src/models/planet"
)

var lock = &sync.Mutex{}
var galaxy *Galaxy

// A Galaxy defines the properties of a solar system Instance
type Galaxy struct {
	Day       int
	Sun       *planet.Planet
	Ferengi   *planet.Planet
	Vulcano   *planet.Planet
	Betasoide *planet.Planet
}

/*
Get Singleton Galaxy instance

 	@return Galaxy Instance
*/
func GetInstance() *Galaxy {
	if galaxy == nil {
		lock.Lock()
		defer lock.Unlock()
		if galaxy == nil {
			var sun = planet.Create("Sun", 0, 0, 0, 0)
			var ferengi = planet.Create("Ferengi", 500, 1, 0, 0)
			var betasoide = planet.Create("Betasoide", 2000, 3, 0, 0)
			var vulcano = planet.Create("Vulcano", 1000, -5, 0, 0)
			galaxy = &Galaxy{
				Sun:       sun,
				Ferengi:   ferengi,
				Betasoide: betasoide,
				Vulcano:   vulcano,
			}
		}
	}
	return galaxy
}

/*
Set galaxy instance day
*/
func (g *Galaxy) SetGalaxyDay(days int) {
	g.Day = galaxy.Day + days
	g.Betasoide.TranslateDays(days)
	g.Ferengi.TranslateDays(days)
	g.Vulcano.TranslateDays(days)
}
