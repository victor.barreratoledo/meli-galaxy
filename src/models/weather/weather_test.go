package weather

import (
	"testing"

	"github.com/stretchr/testify/assert"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/models/galaxy"
)

func TestWeather(t *testing.T) {

	t.Run("Drougth Weather Condition", func(t *testing.T) {
		var _galaxy = galaxy.GetInstance()
		_galaxy.Ferengi.XPosition = float64(0)
		_galaxy.Ferengi.YPosition = float64(0)
		_galaxy.Vulcano.XPosition = float64(0)
		_galaxy.Vulcano.YPosition = float64(0)
		_galaxy.Betasoide.XPosition = float64(0)
		_galaxy.Betasoide.YPosition = float64(0)
		weatherCondition := GetWeatherCondition(_galaxy)
		assert.Equal(t, weatherCondition.String(), DROUGHT.String())
	})

	t.Run("Rainy Weather Condition", func(t *testing.T) {
		var _galaxy = galaxy.GetInstance()
		_galaxy.Ferengi.XPosition = float64(1)
		_galaxy.Ferengi.YPosition = float64(1)
		_galaxy.Vulcano.XPosition = float64(1)
		_galaxy.Vulcano.YPosition = float64(-1)
		_galaxy.Betasoide.XPosition = float64(-1)
		_galaxy.Betasoide.YPosition = float64(0)
		weatherCondition := GetWeatherCondition(_galaxy)
		assert.Equal(t, weatherCondition.String(), RAINY.String())
	})

	t.Run("Optimal Weather Condition", func(t *testing.T) {
		var _galaxy = galaxy.GetInstance()
		_galaxy.Ferengi.XPosition = float64(1)
		_galaxy.Ferengi.YPosition = float64(1)
		_galaxy.Vulcano.XPosition = float64(1)
		_galaxy.Vulcano.YPosition = float64(2)
		_galaxy.Betasoide.XPosition = float64(1)
		_galaxy.Betasoide.YPosition = float64(-5)
		weatherCondition := GetWeatherCondition(_galaxy)
		assert.Equal(t, weatherCondition.String(), OPTIMAL.String())
	})

}
