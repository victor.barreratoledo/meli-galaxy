package weather

import (
	"math"
	"math/big"

	"victor-barrera.com/victorbarrera/galaxy-meli/src/models/galaxy"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/models/planet"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/utils"
)

type Waether int
type Planet = planet.Planet

const (
	DROUGHT Waether = iota
	RAINY
	OPTIMAL
	CLEAR
)

/*
Get string value representation

 	@return weather string value
*/
func (w Waether) String() string {
	return [...]string{"drought", "rainy", "optimal", "clear"}[w]
}

/*
Get Weather for a given galaxy

 	@param galaxy A Galaxy Instance

 	@return weather A Value of Weather Enum
*/
func GetWeatherCondition(galaxy *galaxy.Galaxy) Waether {
	var totalArea = utils.CalcTriangleArea(galaxy.Ferengi.XPosition, galaxy.Ferengi.YPosition, galaxy.Vulcano.XPosition, galaxy.Vulcano.YPosition, galaxy.Betasoide.XPosition, galaxy.Betasoide.YPosition)
	var sunAlignedArea = utils.CalcTriangleArea(galaxy.Ferengi.XPosition, galaxy.Ferengi.YPosition, galaxy.Vulcano.XPosition, galaxy.Vulcano.YPosition, galaxy.Sun.XPosition, galaxy.Sun.YPosition)
	if totalArea == 0 {
		if totalArea == sunAlignedArea && totalArea == 0 && sunAlignedArea == 0 {
			return DROUGHT
		}
		return OPTIMAL
	}
	var calculatedArea = (utils.CalcTriangleArea(galaxy.Ferengi.XPosition, galaxy.Ferengi.YPosition, galaxy.Vulcano.XPosition, galaxy.Vulcano.YPosition, galaxy.Sun.XPosition, galaxy.Sun.YPosition)) +
		(utils.CalcTriangleArea(galaxy.Ferengi.XPosition, galaxy.Ferengi.YPosition, galaxy.Betasoide.XPosition, galaxy.Betasoide.YPosition, galaxy.Sun.XPosition, galaxy.Sun.YPosition)) +
		(utils.CalcTriangleArea(galaxy.Vulcano.XPosition, galaxy.Vulcano.YPosition, galaxy.Betasoide.XPosition, galaxy.Betasoide.YPosition, galaxy.Sun.XPosition, galaxy.Sun.YPosition))
	var diff = totalArea - calculatedArea
	result := big.NewFloat(math.Abs(diff)).Cmp(big.NewFloat(1e-8)) < 1
	if result {
		return RAINY
	} else {
		return CLEAR
	}
}

/*
Get a value that represents rain intesity for a given galaxy

 	@param galaxy A Galaxy Instance

 	@return intensity Rain intensity in a float value representation
*/
func GetRainIntensity(galaxy *galaxy.Galaxy) float64 {
	var perimeter = utils.CalcTrianglePerimeter(galaxy.Ferengi.XPosition, galaxy.Ferengi.YPosition, galaxy.Vulcano.XPosition, galaxy.Vulcano.YPosition, galaxy.Betasoide.XPosition, galaxy.Betasoide.YPosition)
	return perimeter
}
