package data

type Result struct {
	Day              int     `json:"day" bson:"day"`
	WeatherCondition string  `json:"weatherCondition" bson:"weatherCondition"`
	Intensity        float64 `json:"intensity" bson:"intensity"`
	Cycle            int     `json:"cycle" bson:"cycle"`
}

type Summary struct {
	DroughtCycles           float32 `json:"droughtCycles" bson:"droughtCycles"`
	RainyCycles             float32 `json:"rainyCycles" bson:"rainyCycles"`
	OptimalCycles           float32 `json:"optimalCycles" bson:"optimalCycles"`
	Clearcycles             float32 `json:"clearCycles" bson:"clearCycles"`
	DroughtDays             float32 `json:"droughtDays" bson:"droughtDays"`
	RainyDays               float32 `json:"rainyDays" bson:"rainyDays"`
	OptimalDays             float32 `json:"optimalDays" bson:"optimalDays"`
	ClearDays               float32 `json:"clearDays" bson:"clearDays"`
	MaximumRainIntensityDay *Result `json:"maximumRainIntensityDay" bson:"maximumRainIntensityDay"`
}
