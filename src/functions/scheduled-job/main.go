package main

import (
	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/database"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/models/data"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/models/galaxy"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/models/weather"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	var _galaxy = galaxy.GetInstance()
	var years = 270
	var days = years * 365
	var results []data.Result
	var weatherCycles = make(map[string]int)
	var lastDay data.Result
	for day := 0; day <= days; day++ {
		result := &data.Result{Day: day, Cycle: weatherCycles[""]}
		_galaxy.SetGalaxyDay(day)
		dayWeather := weather.GetWeatherCondition(_galaxy)
		result.WeatherCondition = dayWeather.String()
		if lastDay.WeatherCondition != result.WeatherCondition {
			weatherCycles[dayWeather.String()] = weatherCycles[dayWeather.String()] + 1
			lastDay.WeatherCondition = result.WeatherCondition
		}
		result.Cycle = weatherCycles[dayWeather.String()]
		if dayWeather == weather.RAINY {
			intensity := weather.GetRainIntensity(_galaxy)
			result.Intensity = intensity
		} else {
			result.Intensity = 0
		}
		results = append(results, *result)

		if day%1000 == 0 || day == days {
			err := database.BulkResults(results)
			if err != nil {
				return events.APIGatewayProxyResponse{}, err
			}
			results = []data.Result{}
		}

	}

	return events.APIGatewayProxyResponse{
		Body:       "ok",
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(handler)
}
