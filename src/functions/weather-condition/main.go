package main

import (
	"encoding/json"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/database"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/models/galaxy"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/models/weather"
)

type ResponseBody struct {
	Day              int    `json:"day"`
	WeatherCondition string `json:"weatherCondition" `
}

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	dayParam := request.QueryStringParameters["day"]
	if len(dayParam) <= 0 {
		return events.APIGatewayProxyResponse{
			Body:       "day query paramater it's required",
			StatusCode: 400,
		}, nil
	}
	day, err := strconv.Atoi(dayParam)
	if err != nil {
		return events.APIGatewayProxyResponse{}, err
	}
	var weatherContition string
	result, _ := database.GetResultByDay(day)
	if result != nil {
		weatherContition = result.WeatherCondition
	} else {
		var _galaxy = galaxy.GetInstance()
		_galaxy.SetGalaxyDay(day)
		dayWeather := weather.GetWeatherCondition(_galaxy)
		weatherContition = dayWeather.String()
	}
	response := &ResponseBody{
		Day:              day,
		WeatherCondition: weatherContition,
	}
	jsonData, _ := json.Marshal(response)
	return events.APIGatewayProxyResponse{
		Body:       string(jsonData),
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(handler)
}
