package main

import (
	"encoding/json"
	"strconv"

	"github.com/aws/aws-lambda-go/events"
	"github.com/aws/aws-lambda-go/lambda"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/database"
)

func handler(request events.APIGatewayProxyRequest) (events.APIGatewayProxyResponse, error) {
	fromDayParam := request.QueryStringParameters["fromDay"]
	if len(fromDayParam) <= 0 {
		return events.APIGatewayProxyResponse{
			Body:       "fromDay query paramater it's required",
			StatusCode: 400,
		}, nil
	}
	fromDay, err := strconv.Atoi(fromDayParam)
	if err != nil {
		return events.APIGatewayProxyResponse{}, err
	}
	toDayParam := request.QueryStringParameters["toDay"]
	if len(toDayParam) <= 0 {
		return events.APIGatewayProxyResponse{
			Body:       "toDay query paramater it's required",
			StatusCode: 400,
		}, nil
	}
	toDay, err := strconv.Atoi(toDayParam)
	if toDay > 98550 {
		return events.APIGatewayProxyResponse{
			Body:       "Only next 270 years weather forecasts had been calculated, summary query params values need to bee in the next values range: [ 0 - 98550 ]",
			StatusCode: 400,
		}, nil
	}
	if err != nil {
		return events.APIGatewayProxyResponse{}, err
	}
	summary, err := database.Summary(fromDay, toDay)
	if err != nil {
		return events.APIGatewayProxyResponse{}, err
	}
	json, err := json.Marshal(summary)
	if err != nil {
		return events.APIGatewayProxyResponse{}, err
	}
	return events.APIGatewayProxyResponse{
		Body:       string(json),
		StatusCode: 200,
	}, nil
}

func main() {
	lambda.Start(handler)
}
