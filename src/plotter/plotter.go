package plotter

import (
	"log"

	"gonum.org/v1/plot"
	"gonum.org/v1/plot/plotter"
	"gonum.org/v1/plot/vg"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/models/galaxy"
)

func plotGalaxy(galaxy *galaxy.Galaxy) {

	p := plot.New()

	plotter.DefaultLineStyle.Width = vg.Points(0.2)

	p.X.Min = -2000
	p.X.Max = 2000
	p.Y.Min = -2000
	p.Y.Max = 2000

	pts := plotter.XYs{
		{X: galaxy.Sun.XPosition, Y: galaxy.Sun.YPosition},
		{X: galaxy.Ferengi.XPosition, Y: galaxy.Ferengi.YPosition},
		{X: galaxy.Betasoide.XPosition, Y: galaxy.Betasoide.YPosition},
		{X: galaxy.Vulcano.XPosition, Y: galaxy.Vulcano.YPosition},
	}

	pts2 := plotter.XYs{
		{X: galaxy.Ferengi.XPosition, Y: galaxy.Ferengi.YPosition},
		{X: galaxy.Betasoide.XPosition, Y: galaxy.Betasoide.YPosition},
		{X: galaxy.Vulcano.XPosition, Y: galaxy.Vulcano.YPosition},
		{X: galaxy.Ferengi.XPosition, Y: galaxy.Ferengi.YPosition},
	}

	scatter, _ := plotter.NewScatter(pts)
	scatter.Radius = 1
	line, err := plotter.NewLine(pts2)
	if err != nil {
		log.Panic(err)
	}
	p.Add(scatter, line)
	err = p.Save(100, 100, "plot.png")
	if err != nil {
		log.Panic(err)
	}

}
