package database

import (
	"context"
	"fmt"
	"os"
	"strconv"
	"sync"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"go.mongodb.org/mongo-driver/mongo/readpref"
	"victor-barrera.com/victorbarrera/galaxy-meli/src/models/data"
)

var lock = &sync.Mutex{}
var mongoClient *mongo.Client

var (
	DocumentDBEndpoint   string
	DocumentDBPort       int
	DocumentDBUser       string
	DocumentDBPassword   string
	DocumentDBDatabase   string
	DocumentDBCollection string
	RedisClusterEndpoint string
	RedisClusterPort     string
)

/*
Creates a documentDB connection
 	@return err An error instance if something gets wrong, otherwise returns nil value
*/
func connectToDB() error {
	GetEnv()
	mongoURI := fmt.Sprintf("mongodb://%v:%v@%v:%v/?ssl=true&ssl_ca_certs=rds-combined-ca-bundle.pem&&replicaSet=rs0&readPreference=secondaryPreferred&retryWrites=false", DocumentDBUser, DocumentDBPassword, DocumentDBEndpoint, DocumentDBPort)
	//mongoURI := fmt.Sprintf("mongodb://%v:27017/", DocumentDBEndpoint)
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	var err error
	mongoClient, err = mongo.Connect(ctx, options.Client().ApplyURI(mongoURI))
	if err != nil {
		return err
	}
	ctx, _ = context.WithTimeout(context.Background(), 10*time.Second)
	err = mongoClient.Ping(ctx, readpref.SecondaryPreferred())
	if err != nil {
		return err
	}
	return nil
}

/*
Returns docuemntDb connection instance

 	@return mongoClient A *mongo.Client Instance
	@return err An error instance if something gets wrong, otherwise returns nil value
*/
func getInstance() (client *mongo.Client, err error) {
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	if mongoClient == nil || mongoClient.Ping(ctx, readpref.SecondaryPreferred()) != nil {
		lock.Lock()
		defer lock.Unlock()
		if mongoClient == nil || mongoClient.Ping(ctx, readpref.SecondaryPreferred()) != nil {
			err := connectToDB()
			if err != nil {
				return nil, err
			}
		}
	}
	return mongoClient, nil
}

/*
Saves Multiple Results in documentDB datasource

 	@param reuslt A Result Instance
 	@return err An error instance if something gets wrong, otherwise returns nil value
*/
func BulkResults(results []data.Result) error {
	mongoClient, err := getInstance()
	defer mongoClient.Disconnect(context.Background())
	if err != nil {
		return err
	}
	col := mongoClient.Database(DocumentDBDatabase).Collection(DocumentDBCollection)
	var operations []mongo.WriteModel
	bulkOption := options.BulkWriteOptions{}

	for _, result := range results {
		bsonDoc, err := bson.Marshal(result)
		if err != nil {
			return err
		}
		operation := mongo.NewReplaceOneModel()
		operation.SetFilter(bson.M{"day": result.Day})
		operation.SetReplacement(bsonDoc)
		operation.SetUpsert(true)
		operations = append(operations, operation)
	}
	bulkOption.SetOrdered(false)
	ctx, _ := context.WithTimeout(context.Background(), 720*time.Second)
	_, err = col.BulkWrite(ctx, operations, &bulkOption)
	if err != nil {
		return err
	}
	return nil
}

/*
	Returns resulr for specific day

 	@param reuslt A Result Instance
 	@return err An error instance if something gets wrong, otherwise returns nil value
*/
func GetResultByDay(day int) (*data.Result, error) {
	mongoClient, err := getInstance()
	defer mongoClient.Disconnect(context.Background())
	if err != nil {
		return nil, err
	}
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	col := mongoClient.Database(DocumentDBDatabase).Collection(DocumentDBCollection)
	cursor := col.FindOne(ctx, bson.M{"day": day})
	result := &data.Result{Cycle: 0, Day: 0}
	err = cursor.Decode(&result)
	if err != nil {
		return nil, err
	}
	return result, nil
}

/*
	Returns rainy day with the most intesity

 	@param reuslt A Result Instance
 	@return err An error instance if something gets wrong, otherwise returns nil value
*/
func GetMaximumRainIntesity() (*data.Result, error) {
	mongoClient, err := getInstance()
	defer mongoClient.Disconnect(context.Background())
	if err != nil {
		return &data.Result{}, err
	}
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	col := mongoClient.Database(DocumentDBDatabase).Collection(DocumentDBCollection)
	options := options.Find()
	options.SetLimit(1)
	options.SetSort(bson.M{"intensity": -1})
	cursor, _ := col.Find(ctx, bson.M{}, options)
	defer cursor.Close(context.Background())
	result := &data.Result{Cycle: 0, Day: 0}
	for cursor.Next(context.TODO()) {
		err := cursor.Decode(&result)
		if err != nil {
			return &data.Result{}, err
		}
	}
	return result, nil
}

func GetLastSavedDay() (*data.Result, error) {
	mongoClient, err := getInstance()
	defer mongoClient.Disconnect(context.Background())
	if err != nil {
		return &data.Result{}, err
	}
	ctx, _ := context.WithTimeout(context.Background(), 5*time.Second)
	col := mongoClient.Database(DocumentDBDatabase).Collection(DocumentDBCollection)
	options := options.Find()
	options.SetLimit(1)
	options.SetSort(bson.M{"day": -1})
	cursor, _ := col.Find(ctx, bson.M{}, options)
	defer cursor.Close(context.Background())
	result := &data.Result{Cycle: 0, Day: 0}
	for cursor.Next(context.TODO()) {
		err := cursor.Decode(&result)
		if err != nil {
			return &data.Result{}, err
		}
	}

	return result, nil
}

/*
Returns summary from a renge of given days
*/
func Summary(fromDay, toDay int) (result data.Summary, err error) {
	mongoClient, err := getInstance()
	defer mongoClient.Disconnect(context.Background())
	if err != nil {
		return data.Summary{}, err
	}
	ctx, _ := context.WithTimeout(context.Background(), 10*time.Second)
	col := mongoClient.Database(DocumentDBDatabase).Collection(DocumentDBCollection)
	pipeline := mongo.Pipeline{
		{{"$match", bson.D{
			{"$and", bson.A{bson.M{"day": bson.M{"$gte": fromDay}}, bson.M{"day": bson.M{"$lte": toDay}}}},
		}}},
		{{"$sort", bson.D{
			{"cycle", -1},
		}}},
		{{"$group", bson.D{
			{"_id", "$weatherCondition"},
			{"daysArray", bson.D{{"$push", bson.M{"day": "$day", "intensity": "$intensity", "cycle": "$cycle"}}}},
		}}},
		{{"$group", bson.D{
			{"_id", nil},
			{"data", bson.D{
				{"$push", bson.D{
					{"k", "$_id"},
					{"v", "$daysArray"},
				}}}},
		}}},
		{{"$replaceRoot", bson.D{
			{"newRoot", bson.D{{"$arrayToObject", "$data"}}},
		}}},
		{{"$addFields", bson.D{
			{"firstRainyCycle", bson.D{{"$cond", bson.M{"if": bson.M{"$isArray": "$rainy"}, "then": bson.M{"$arrayElemAt": bson.A{"$rainy.cycle", 0}}, "else": 0}}}},
			{"lastRainyCycle", bson.D{{"$cond", bson.M{"if": bson.M{"$isArray": "$rainy"}, "then": bson.M{"$arrayElemAt": bson.A{"$rainy.cycle", -1}}, "else": 1}}}},
			{"firstDroughtCycle", bson.D{{"$cond", bson.M{"if": bson.M{"$isArray": "$drought"}, "then": bson.M{"$arrayElemAt": bson.A{"$drought.cycle", 0}}, "else": 0}}}},
			{"lastDroughtCycle", bson.D{{"$cond", bson.M{"if": bson.M{"$isArray": "$drought"}, "then": bson.M{"$arrayElemAt": bson.A{"$drought.cycle", -1}}, "else": 1}}}},
			{"firstOptimalCycle", bson.D{{"$cond", bson.M{"if": bson.M{"$isArray": "$optimal"}, "then": bson.M{"$arrayElemAt": bson.A{"$optimal.cycle", 0}}, "else": 0}}}},
			{"lastOptimalCycle", bson.D{{"$cond", bson.M{"if": bson.M{"$isArray": "$optimal"}, "then": bson.M{"$arrayElemAt": bson.A{"$optimal.cycle", -1}}, "else": 1}}}},
			{"firstClearCycle", bson.D{{"$cond", bson.M{"if": bson.M{"$isArray": "$clear"}, "then": bson.M{"$arrayElemAt": bson.A{"$clear.cycle", 0}}, "else": 0}}}},
			{"lastClearCycle", bson.D{{"$cond", bson.M{"if": bson.M{"$isArray": "$clear"}, "then": bson.M{"$arrayElemAt": bson.A{"$clear.cycle", -1}}, "else": 1}}}},
		}}},
		{{"$project", bson.D{
			{"droughtCycles", bson.D{{"$subtract", bson.A{"$firstDroughtCycle", bson.M{"$subtract": bson.A{"$lastDroughtCycle", 1}}}}}},
			{"rainyCycles", bson.D{{"$subtract", bson.A{"$firstRainyCycle", bson.M{"$subtract": bson.A{"$lastRainyCycle", 1}}}}}},
			{"optimalCycles", bson.D{{"$subtract", bson.A{"$firstOptimalCycle", bson.M{"$subtract": bson.A{"$lastOptimalCycle", 1}}}}}},
			{"clearCycles", bson.D{{"$subtract", bson.A{"$firstClearCycle", bson.M{"$subtract": bson.A{"$lastClearCycle", 1}}}}}},
			{"droughtDays", bson.D{{"$cond", bson.M{"if": bson.M{"$isArray": "$drought"}, "then": bson.M{"$size": "$drought"}, "else": 0}}}},
			{"rainyDays", bson.D{{"$cond", bson.M{"if": bson.M{"$isArray": "$rainy"}, "then": bson.M{"$size": "$rainy"}, "else": 0}}}},
			{"optimalDays", bson.D{{"$cond", bson.M{"if": bson.M{"$isArray": "$optimal"}, "then": bson.M{"$size": "$optimaly"}, "else": 0}}}},
			{"clearDays", bson.D{{"$cond", bson.M{"if": bson.M{"$isArray": "$clear"}, "then": bson.M{"$size": "$clear"}, "else": 0}}}},
		}}},
	}
	results, err := col.Aggregate(ctx, pipeline)
	if err != nil {
		return result, err
	}
	var summary data.Summary
	for results.Next(context.TODO()) {
		err := results.Decode(&summary)
		if err != nil {
			return result, err
		}
	}
	results.Close(context.Background())
	maximumRainIntensityDay, err := GetMaximumRainIntesity()
	if err != nil {
		return result, err
	}
	summary.MaximumRainIntensityDay = maximumRainIntensityDay
	return summary, nil
}

/**
 * Assign environment variables
 */
func GetEnv() {
	port, _ := strconv.Atoi(os.Getenv("DOCUMENTDB_CLUSTER_PORT"))
	DocumentDBEndpoint = os.Getenv("DOCUMENTDB_CLUSTER_ENDPOINT")
	DocumentDBPort = port
	DocumentDBUser = os.Getenv("DOCUMENTDB_CLUSTER_USERNAME")
	DocumentDBPassword = os.Getenv("DOCUMENTDB_CLUSTER_PASSWORD")
	DocumentDBDatabase = "database"
	DocumentDBCollection = "collection"
	RedisClusterEndpoint = os.Getenv("REDIS_CLUSTER_ENDPOINT")
	RedisClusterPort = os.Getenv("REDIS_CLUSTER_PORT")
}
