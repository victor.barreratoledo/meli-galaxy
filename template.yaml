AWSTemplateFormatVersion: "2010-09-09"
Transform: AWS::Serverless-2016-10-31
Description: >
  Server Aplication Model Template

# More info about Globals: https://github.com/awslabs/serverless-application-model/blob/master/docs/globals.rst
Globals:
  Function:
    Timeout: 850
    MemorySize: 3000

Parameters:
  DocDBUsername:
    Type: String
    Description: Username for the Amazon DocumentDB cluster

  DocDBPassword:
    Type: String
    Description: Password for the Amazon DocumentDB cluster
    NoEcho: true
    MinLength: 8

Mappings:
  SubnetConfig:
    VPC:
      CIDR: "10.0.0.0/16"
    PublicOne:
      CIDR: "10.0.0.0/24"
    PublicTwo:
      CIDR: "10.0.1.0/24"
    PublicThree:
      CIDR: "10.0.2.0/24"
    PrivateOne:
      CIDR: "10.0.100.0/24"
    PrivateTwo:
      CIDR: "10.0.101.0/24"
    PrivateThree:
      CIDR: "10.0.102.0/24"

Resources:
  VPC:
    Type: AWS::EC2::VPC
    Properties:
      EnableDnsSupport: true
      EnableDnsHostnames: true
      CidrBlock: !FindInMap ["SubnetConfig", "VPC", "CIDR"]
      Tags:
        - Key: Name
          Value: !Sub VPC-${AWS::StackName}

  PublicSubnetOne:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone:
        Fn::Select:
          - 0
          - Fn::GetAZs: { Ref: "AWS::Region" }
      VpcId: !Ref VPC
      CidrBlock: !FindInMap ["SubnetConfig", "PublicOne", "CIDR"]
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Sub PublicOne-${AWS::StackName}

  PublicSubnetTwo:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone:
        Fn::Select:
          - 1
          - Fn::GetAZs: { Ref: "AWS::Region" }
      VpcId: !Ref VPC
      CidrBlock: !FindInMap ["SubnetConfig", "PublicTwo", "CIDR"]
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Sub PublicTwo-${AWS::StackName}

  PublicSubnetThree:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone:
        Fn::Select:
          - 2
          - Fn::GetAZs: { Ref: "AWS::Region" }
      VpcId: !Ref VPC
      CidrBlock: !FindInMap ["SubnetConfig", "PublicThree", "CIDR"]
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Sub PublicThree-${AWS::StackName}

  PrivateSubnetOne:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone:
        Fn::Select:
          - 0
          - Fn::GetAZs: { Ref: "AWS::Region" }
      VpcId: !Ref VPC
      CidrBlock: !FindInMap ["SubnetConfig", "PrivateOne", "CIDR"]
      Tags:
        - Key: Name
          Value: !Sub PrivateOne-${AWS::StackName}

  PrivateSubnetTwo:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone:
        Fn::Select:
          - 1
          - Fn::GetAZs: { Ref: "AWS::Region" }
      VpcId: !Ref VPC
      CidrBlock: !FindInMap ["SubnetConfig", "PrivateTwo", "CIDR"]
      Tags:
        - Key: Name
          Value: !Sub PrivateTwo-${AWS::StackName}

  PrivateSubnetThree:
    Type: AWS::EC2::Subnet
    Properties:
      AvailabilityZone:
        Fn::Select:
          - 2
          - Fn::GetAZs: { Ref: "AWS::Region" }
      VpcId: !Ref VPC
      CidrBlock: !FindInMap ["SubnetConfig", "PrivateThree", "CIDR"]
      Tags:
        - Key: Name
          Value: !Sub PrivateThree-${AWS::StackName}

  AppSecurityGroup:
    Type: AWS::EC2::SecurityGroup
    Properties:
      GroupDescription: Amazon DocumentDB Security Group
      VpcId: !Ref VPC
      SecurityGroupIngress:
        - IpProtocol: tcp
          CidrIp: 10.0.0.0/16
          FromPort: 27017
          ToPort: 27017
        - IpProtocol: tcp
          FromPort: 6379
          ToPort: 6379

  DocumentDBSubnetGroup:
    Type: AWS::DocDB::DBSubnetGroup
    Properties:
      DBSubnetGroupDescription: Subnet Group for DocumentDB
      SubnetIds:
        - !Ref PrivateSubnetOne
        - !Ref PrivateSubnetTwo
        - !Ref PrivateSubnetThree

  DocumentDBCluster:
    Type: AWS::DocDB::DBCluster
    Properties:
      DBClusterIdentifier: documentdb-sam
      MasterUsername: !Ref DocDBUsername
      MasterUserPassword: !Ref DocDBPassword
      DBSubnetGroupName: !Ref DocumentDBSubnetGroup
      StorageEncrypted: yes
      Tags:
        - Key: Name
          Value: !Sub DocumentDB-${AWS::StackName}
      VpcSecurityGroupIds:
        - !Ref AppSecurityGroup
    DependsOn: VPC

  DocumentDBInstanceWriter:
    Type: AWS::DocDB::DBInstance
    Properties:
      DBClusterIdentifier: !Ref DocumentDBCluster
      DBInstanceClass: db.r5.large
      Tags:
        - Key: Name
          Value: !Sub DocumentDBInstance-${AWS::StackName}

  DocumentDBInstanceReader1:
    Type: AWS::DocDB::DBInstance
    Properties:
      DBClusterIdentifier: !Ref DocumentDBCluster
      DBInstanceClass: db.r5.large
      Tags:
        - Key: Name
          Value: !Sub DocumentDBInstance-${AWS::StackName}

  DocumentDBInstanceReader2:
    Type: AWS::DocDB::DBInstance
    Properties:
      DBClusterIdentifier: !Ref DocumentDBCluster
      DBInstanceClass: db.r5.large
      Tags:
        - Key: Name
          Value: !Sub DocumentDBInstance-${AWS::StackName}


  SheduledJob:
    Type: "AWS::Serverless::Function"
    # DependsOn: RedisCluster
    Properties:
      CodeUri: src/functions/scheduled-job
      Handler: src/functions/scheduled-job
      Runtime: go1.x
      Tracing: Active
      Events:
        ScheduledEvent:
          Type: Schedule
          Properties:
            Schedule: rate(700 days)
            Description: Job scheduling
            Enabled: True
      Environment:
        Variables:
          DOCUMENTDB_CLUSTER_ENDPOINT: !GetAtt DocumentDBCluster.Endpoint
          DOCUMENTDB_CLUSTER_PORT: !GetAtt DocumentDBCluster.Port
          DOCUMENTDB_CLUSTER_USERNAME: !Ref DocDBUsername
          DOCUMENTDB_CLUSTER_PASSWORD: !Ref DocDBPassword
          # ELASTICACHE_ENDPOINT: !GetAtt RedisCluster.ConfigurationEndPoint.Address
          # ELASTICACHE_PORT: !GetAtt RedisCluster.ConfigurationEndPoint.Port
      VpcConfig:
        SecurityGroupIds:
          - !Ref AppSecurityGroup
        SubnetIds:
          - !Ref PrivateSubnetOne
          - !Ref PrivateSubnetTwo
          - !Ref PrivateSubnetThree

      Policies:
        - Statement:
            - Sid: AWSLambdaVPCAccessExecutionRole
              Effect: Allow
              Action:
                - logs:CreateLogGroup
                - logs:CreateLogStream
                - logs:PutLogEvents
                - ec2:CreateNetworkInterface
                - ec2:DescribeNetworkInterfaces
                - ec2:DeleteNetworkInterface
              Resource: "*"
        - AWSXRayDaemonWriteAccess
        - CloudWatchPutMetricPolicy: {}

  GetWeatherCondition:
    Type: "AWS::Serverless::Function"
    # DependsOn: RedisCluster
    Properties:
      CodeUri: src/functions/weather-condition
      Handler: src/functions/weather-condition
      Runtime: go1.x
      Tracing: Active
      Events:
        CatchAll:
          Type: Api
          Properties:
            Path: /weather-condition
            Method: GET
      Environment:
        Variables:
          DOCUMENTDB_CLUSTER_ENDPOINT: !GetAtt DocumentDBCluster.Endpoint
          DOCUMENTDB_CLUSTER_PORT: !GetAtt DocumentDBCluster.Port
          DOCUMENTDB_CLUSTER_USERNAME: !Ref DocDBUsername
          DOCUMENTDB_CLUSTER_PASSWORD: !Ref DocDBPassword
          # ELASTICACHE_ENDPOINT: !GetAtt RedisCluster.ConfigurationEndPoint.Address
          # ELASTICACHE_PORT: !GetAtt RedisCluster.ConfigurationEndPoint.Port
      VpcConfig:
        SecurityGroupIds:
          - !Ref AppSecurityGroup
        SubnetIds:
          - !Ref PrivateSubnetOne
          - !Ref PrivateSubnetTwo
          - !Ref PrivateSubnetThree
      Policies:
        - Statement:
            - Sid: AWSLambdaVPCAccessExecutionRole
              Effect: Allow
              Action:
                - logs:CreateLogGroup
                - logs:CreateLogStream
                - logs:PutLogEvents
                - ec2:CreateNetworkInterface
                - ec2:DescribeNetworkInterfaces
                - ec2:DeleteNetworkInterface
              Resource: "*"
        - AWSXRayDaemonWriteAccess
        - CloudWatchPutMetricPolicy: {}


  GetSummary:
    Type: "AWS::Serverless::Function"
    # DependsOn: RedisCluster
    Properties:
      CodeUri: src/functions/summary
      Handler: src/functions/summary
      Runtime: go1.x
      Tracing: Active
      Events:
        CatchAll:
          Type: Api
          Properties:
            Path: /summary
            Method: GET
      Environment:
        Variables:
          DOCUMENTDB_CLUSTER_ENDPOINT: !GetAtt DocumentDBCluster.Endpoint
          DOCUMENTDB_CLUSTER_PORT: !GetAtt DocumentDBCluster.Port
          DOCUMENTDB_CLUSTER_USERNAME: !Ref DocDBUsername
          DOCUMENTDB_CLUSTER_PASSWORD: !Ref DocDBPassword
          # ELASTICACHE_ENDPOINT: !GetAtt RedisCluster.ConfigurationEndPoint.Address
          # ELASTICACHE_PORT: !GetAtt RedisCluster.ConfigurationEndPoint.Port
      VpcConfig:
        SecurityGroupIds:
          - !Ref AppSecurityGroup
        SubnetIds:
          - !Ref PrivateSubnetOne
          - !Ref PrivateSubnetTwo
          - !Ref PrivateSubnetThree

      Policies:
        - Statement:
            - Sid: AWSLambdaVPCAccessExecutionRole
              Effect: Allow
              Action:
                - logs:CreateLogGroup
                - logs:CreateLogStream
                - logs:PutLogEvents
                - ec2:CreateNetworkInterface
                - ec2:DescribeNetworkInterfaces
                - ec2:DeleteNetworkInterface
              Resource: "*"
        - AWSXRayDaemonWriteAccess
        - CloudWatchPutMetricPolicy: {}

Outputs:
  API:
    Description: "API Gateway endpoint URL for Prod environment for First Function"
    Value: !Sub "https://${ServerlessRestApi}.execute-api.${AWS::Region}.amazonaws.com/Prod/"
  ClusterId:
    Value: !Ref DocumentDBCluster
  DocumentDBClusterEndpoint:
    Value: !GetAtt DocumentDBCluster.Endpoint
  DocumentDBClusterPort:
    Value: !GetAtt DocumentDBCluster.Port
  # RedisClusterEndpoint:
  #   Value: !GetAtt RedisCluster.ConfigurationEndPoint.Address
  # RedisClusterPort:
  #   Value: !GetAtt RedisCluster.ConfigurationEndPoint.Port
