# Meli-Galaxy

This is a template for mercadolibre-galaxy app - Below is a brief explanation of what i had structured for you:

```bash
.
├── Makefile                    <-- Makefile to automate testing & building 
├── README.md                   <-- This instructions file
├── docs                        <-- Source Code Folder
│   ├── guides                  <-- 
│       └── .....png            <-- Guided deployment with images 
│   └── infra                   <-- Utils Package Folder
│       └── infra-aws.svg       <-- AWs Serverless Architecture Diagram
├── src                         <-- Source Code Folder
│   ├── cache                   <-- 
│       └── cache.go            <-- 
│   ├── database                <-- Database Package Folder
│       └── database.go         <-- Database Methods Used With-In Lambda Functions
│   ├── functions               <-- Functions folder
│       ├── whater-condition    <-- Lambda Folder
│           ├── main.go         <-- Lambda Function Code
│           └── main_test.go    <-- Unit tests
│       ├── sheduled-job        <-- Lambda Folder
│           ├── main.go         <-- Lambda Function Code
│           └── main_test.go    <-- Unit tests
│       └── summary             <-- Lambda Folder
│           ├── main.go         <-- Lambda Function Code
│           └── main_test.go    <-- Unit tests
│   ├── models                  <-- Models Folder
│       ├── data                <-- Data Package Folder
│           └── data.go         <-- 
│       ├── galaxy              <-- Galaxy Package Folder
│           └── galaxy.go       <-- 
│       ├── planet              <-- Planet Package Folder
│           └── planet.go       <-- 
│       └── weather             <-- Weather Package Folder
│           └── weather.go      <-- 
│   ├── plotter                 <-- Plotter Package Folder
│       └── plotter.go          <-- 
│   └── utils                   <-- Utils Package Folder
│       └── utils.go            <-- Math/Helper Funtions Used With-In Lambda Functions
└── template.yaml               <-- AWS SAM CLI Template
```


## API Specification

###API Gateway Endpoint

* [https://jgk96sww2j.execute-api.us-west-2.amazonaws.com/Prod/](https://jgk96sww2j.execute-api.us-west-2.amazonaws.com/Prod/)

API Services

Name | Method | Path | Dir | Example
----|----|------------|-----------|----
Weather condition | GET | /weahter_condtion/?day=DAY/ |[src/functions/weather-condition](https://gitlab.com/victor.barreratoledo/meli-galaxy/-/tree/master/src/functions/weather-condition) | [Example](https://jgk96sww2j.execute-api.us-west-2.amazonaws.com/Prod/weather-condition/?day=1)
Summary | GET | /summary?fromDay=FROM_DAY&toDay=TO_DAY/ | [src/functions/summary](https://gitlab.com/victor.barreratoledo/meli-galaxy/-/tree/master/src/functions/summary) |[Example](https://jgk96sww2j.execute-api.us-west-2.amazonaws.com/Prod/summary/?fromDay=0&toDay=3650)

Jobs 

Name | Description | Dir
-----|-------------| ------
Sheduled Job| Calculates 270 years weather forecasts | [src/functions/scheduled-job](https://gitlab.com/victor.barreratoledo/meli-galaxy/-/tree/master/src/functions/scheduled-job)

## Services Specification

###Weather condition

###Params

- **day**: The day to predict 

###Response

Example:

```json
{
    "day": 1,
    "weatherCondition": "clear"
}
```
- **droughtCycles**: The day
- **rainyCycles**: The day weather condition

###Summary

###Params

- **fromDay**: The day to start from 
- **toDay**: The last day to consider

###Response


Example:

```json
{
    "droughtCycles": 41,
    "rainyCycles": 81,
    "optimalCycles": 0,
    "clearCycles": 122,
    "droughtDays": 41,
    "rainyDays": 1168,
    "optimalDays": 0,
    "clearDays": 2442,
    "maximumRainIntensityDay": {
        "day": 64368,
        "weatherCondition": "rainy",
        "intensity": 6262.300354242003,
        "cycle": 1431
    }
}
```

- **droughtCycles**: Drought periods total count between the given days
- **rainyCycles**: Rainy periodstotal count between the given days
- **optimalCycles**: Optimal periodstotal count between the given days
- **clearCycles**: Clear periods total count between the given days
- **droughtDays**: Drougth Days total count between the given days
- **rainyDays**: drougth Days total count between the given days
- **optimalDays**: Optimal Days total count between the given days
- **clearDays**: Clear Days total count between the given days
- **maximumRainIntensityDay** :
  - **day** : The day with more rain intensity
  -  **weatherCondition** : The day weather condition
  -  **intensity**: The rain intesity
  -  **cycle**: The day weather condition cycle


####Summary

#####Params

- **fromDay**: The day to predict 

- **toDay**: The day to predict 

## Requirements

* AWS CLI already configured with Administrator permission
* [Docker installed](https://www.docker.com/community-edition)
* [Golang](https://golang.org)
* SAM CLI - [Install the SAM CLI](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html)

## Setup process

### Testing

I use `testing` package that is built-in in Golang and you can simply run the following command to run tests:

```shell
    go test -v ./...
```

### Building 

In this example i use the built-in `sam build` to automatically download all the dependencies and package our build target.   
Read more about [SAM Build here](https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/sam-cli-command-reference-sam-build.html) 

The `sam build` and automated commands are wrapped inside of the `Makefile`. To execute this simply run
 
```shell
    sam build
```

## Packaging and deployment

AWS Lambda Golang runtime requires a flat folder with the executable generated on build step. SAM will use `CodeUri` property to know where to look up for the application:

```yaml
...
    GetWeatherConditionFunction:
        Type: AWS::Serverless::Function
        Properties:
            CodeUri: src/funtions/weather-condition
            ...
```

To deploy the application, run the following in your shell:

```bash
    sam deploy --guided
```

Next, you can use the following resources to know more about Serverless applications:

* [AWS Serverless Application Repository](https://aws.amazon.com/serverless/serverlessrepo/)
