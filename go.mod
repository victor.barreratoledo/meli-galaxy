module victor-barrera.com/victorbarrera/galaxy-meli

go 1.14

require (
	github.com/aws/aws-lambda-go v1.25.0
	github.com/stretchr/testify v1.6.1
	go.mongodb.org/mongo-driver v1.6.0
	gonum.org/v1/plot v0.9.0
)
